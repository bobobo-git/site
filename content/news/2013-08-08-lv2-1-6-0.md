Title: LV2 1.6.0
Date: 2013-08-08 22:00
Slug: lv2-1-6-0
Author: drobilla

[LV2 1.6.0](http://lv2plug.in/spec/lv2-1.6.0.tar.bz2) has been released.  LV2 is a plugin standard for audio systems. It defines a minimal yet extensible C API for plugin code and a format for plugin "bundles".  See <http://lv2plug.in> for more information.

Changes:

 * Fix lv2specgen usage from command line.
 * Fix port indices of metronome example.
 * Upgrade to waf 1.7.11.
 * atom: Fix crash in forge.h when pushing atoms to a full buffer.
 * ui: Add idle interface so native UIs and foreign toolkits can drive their event loops.
 * ui: Add ui:updateRate property.
