Title: LV2 1.8.0
Date: 2014-01-03 23:00
Slug: lv2-1-8-0
Author: drobilla

[LV2 1.8.0](http://lv2plug.in/spec/lv2-1.8.0.tar.bz2) has been released.  LV2 is a plugin standard for audio systems. It defines a minimal yet extensible C API for plugin code and a format for plugin "bundles".  See <http://lv2plug.in> for more information.

Changes:

 * Add scope example plugin from Robin Gareus.
 * Install lv2specgen for use by other projects.
 * atom: Make lv2_atom_*_is_end() arguments const.
 * core: Add lv2:prototype for property inheritance.
 * log: Add missing include string.h to logger.h for memset.
 * lv2specgen: Fix links to externally defined terms.
 * ui: Fix LV2_UI_INVALID_PORT_INDEX identifier in documentation.
