Title: LV2 1.10.0
Date: 2014-08-07 22:00
Slug: lv2-1-10-0
Author: drobilla

[LV2 1.10.0](http://lv2plug.in/spec/lv2-1.10.0.tar.bz2) has been released.  LV2 is a plugin standard for audio systems. It defines a minimal yet extensible C API for plugin code and a format for plugin "bundles".  See <http://lv2plug.in> for more information.

Changes:

 * Fix -Wconversion warnings in headers.
 * Upgrade to waf 1.7.16.
 * atom: Add lv2_atom_forge_is_object_type() and lv2_atom_forge_is_blank() to ease backwards compatibility.
 * atom: Add lv2_atom_forge_key() for terser object writing.
 * atom: Add lv2_atom_sequence_clear() and lv2_atom_sequence_append_event() helper functions.
 * atom: Deprecate Blank and Resource in favour of just Object.
 * core: Clarify lv2_descriptor() and lv2_lib_descriptor() documentation.
 * event: Minor documentation improvements.
 * lv2specgen: Display deprecated warning on classes marked owl:deprecated.
 * patch: Add patch:sequenceNumber for associating replies with requests.
 * ui: Add show interface so UIs can gracefully degrade to separate windows if hosts can not use their widget directly.
 * ui: Fix identifier typos in documentation.
