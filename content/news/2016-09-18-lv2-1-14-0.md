Title: LV2 1.14.0
Date: 2016-09-18 22:00
Slug: lv2-1-14-0
Author: drobilla

[LV2 1.14.0](http://lv2plug.in/spec/lv2-1.14.0.tar.bz2) has been released.  LV2 is a plugin standard for audio systems. It defines a minimal yet extensible C API for plugin code and a format for plugin "bundles".  See <http://lv2plug.in> for more information.

Changes:

 * buf-size: Add bufsz:coarseBlockLength feature.
 * buf-size: Add bufsz:nominalBlockLength option.
 * core: Add lv2:enabled designation.
 * core: Add lv2_util.h with lv2_features_data() and lv2_features_query().
 * eg-sampler: Fix handling of state file paths.
 * eg-sampler: Support thread-safe state restoration.
 * eg-scope: Don't feed back UI state updates.
 * log: Add lv2_log_logger_set_map() for changing the URI map of an existing logger.
 * state: Add LV2_STATE_ERR_NO_SPACE status flag.
 * state: Add state:threadSafeRestore feature for dropout-free state restoration.
 * time: Define LV2_TIME_PREFIX.
 * ui: Add missing property labels.
 * ui: Improve documentation.
