Title:
save_as: index.html

LV2 is an extensible open standard for audio plugins.
LV2 has a simple core interface,
which is accompanied by extensions that add more advanced functionality.

Many types of plugins can be built with LV2, including audio effects, synthesizers, and control processors for modulation and automation.
Extensions support more powerful features, such as:

 * Platform-native UIs
 * Network-transparent plugin control
 * Portable and archivable persistent state
 * Non-realtime tasks (like file loading) with sample-accurate export
 * Semantic control with meaningful control designations and value units

The LV2 specification itself as well as the accompanying libraries are permissively licensed free software, with support for all major platforms.

<a class="biglink" href="http://lv2plug.in/spec/lv2-1.18.2.tar.bz2">Download LV2 1.18.2</a>
<a class="siglink" href="http://lv2plug.in/spec/lv2-1.18.2.tar.bz2.asc">sig</a>

Learn
-----

* [Why LV2?](pages/why-lv2.html)
* [Developing with LV2](pages/developing.html)

Discover
--------

* [Projects using LV2](pages/projects.html)
* [Mailing list](http://lists.lv2plug.in/listinfo.cgi/devel-lv2plug.in)
* [Chat](http://webchat.freenode.net/?channels=lv2)
